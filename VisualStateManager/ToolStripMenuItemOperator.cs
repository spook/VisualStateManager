﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualStateManager
{
    internal class ToolStripMenuItemOperator : BaseControlOperator
    {
        public override Type AcceptedType
        {
            get
            {
                return typeof(ToolStripMenuItem);
            }
        }

        public override void SetEnabled(object control, bool value)
        {
            SafePerform<ToolStripMenuItem>(control, p => p.Enabled = value);
        }

        public override void SetChecked(object control, bool value)
        {
            SafePerform<ToolStripMenuItem>(control, p => p.Checked = value);
        }

        public override void SetVisible(object control, bool value)
        {
            SafePerform<ToolStripMenuItem>(control, p => p.Visible = value);
        }

        public override void HookClickEvent(object control, EventHandler clickHandler)
        {
            SafePerform<ToolStripMenuItem>(control, p => p.Click += clickHandler);
        }

        public override void UnhookClickEvent(object control, EventHandler clickHandler)
        {
            SafePerform<ToolStripMenuItem>(control, p => p.Click -= clickHandler);
        }
    }
}
