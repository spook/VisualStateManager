﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualStateManager
{
    public class VisualStateController : BaseVisualStateManager
    {
        public VisualStateController()
            : base()
        {

        }

        public VisualStateController(params object[] controls)
            : base(controls)
        {

        }

        public VisualStateController(BaseCondition newEnabledCondition, params object[] controls)
            : base(newEnabledCondition, controls)
        {

        }

        public VisualStateController(BaseCondition newEnabledCondition,
            BaseCondition newCheckedCondition,
            params object[] controls)
            : base(newEnabledCondition, newCheckedCondition, controls)
        {

        }

        public VisualStateController(BaseCondition newEnabledCondition,
            BaseCondition newCheckedCondition,
            BaseCondition newVisibleCondition,
            params object[] controls)
            : base(newEnabledCondition,
                newCheckedCondition,
                newVisibleCondition,
                controls)
        {

        }
    }
}
