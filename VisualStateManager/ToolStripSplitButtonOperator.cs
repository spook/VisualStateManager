﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualStateManager
{
    internal class ToolStripSplitButtonOperator : BaseControlOperator
    {
        public override Type AcceptedType
        {
            get
            {
                return typeof(ToolStripSplitButton);
            }
        }

        public override void SetEnabled(object control, bool value)
        {
            SafePerform<ToolStripSplitButton>(control, p => p.Enabled = value);
        }

        public override void SetChecked(object control, bool value)
        {
            // ToolStripSplitButton does not support Checked
        }

        public override void SetVisible(object control, bool value)
        {
            SafePerform<ToolStripSplitButton>(control, p => p.Visible = value);
        }

        public override void HookClickEvent(object control, EventHandler clickHandler)
        {
            SafePerform<ToolStripSplitButton>(control, p => p.ButtonClick += clickHandler);
        }

        public override void UnhookClickEvent(object control, EventHandler clickHandler)
        {
            SafePerform<ToolStripSplitButton>(control, p => p.ButtonClick -= clickHandler);
        }
    }
}
