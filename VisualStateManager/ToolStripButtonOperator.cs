﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualStateManager
{
    internal class ToolStripButtonOperator : BaseControlOperator
    {
        public override Type AcceptedType
        {
            get
            {
                return typeof(ToolStripButton);
            }
        }

        public override void SetEnabled(object control, bool value)
        {
            SafePerform<ToolStripButton>(control, p => p.Enabled = value);
        }

        public override void SetChecked(object control, bool value)
        {
            SafePerform<ToolStripButton>(control, p => p.Checked = value);
        }

        public override void SetVisible(object control, bool value)
        {
            SafePerform<ToolStripButton>(control, p => p.Visible = value);
        }

        public override void HookClickEvent(object control, EventHandler clickHandler)
        {
            SafePerform<ToolStripButton>(control, p => p.Click += clickHandler);
        }

        public override void UnhookClickEvent(object control, EventHandler clickHandler)
        {
            SafePerform<ToolStripButton>(control, p => p.Click -= clickHandler);
        }
    }
}
