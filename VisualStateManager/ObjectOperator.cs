﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace VisualStateManager
{
    internal class ObjectOperator : BaseControlOperator
    {
        public override Type AcceptedType
        {
            get
            {
                return typeof(object);
            }
        }

        public override void SetEnabled(object control, bool value)
        {
            PropertyInfo info = control.GetType().GetProperty("Enabled");
            if (info != null && info.CanWrite)
                info.SetValue(control, value, null);
        }

        public override void SetChecked(object control, bool value)
        {
            PropertyInfo info = control.GetType().GetProperty("Checked");
            if (info != null && info.CanWrite)
                info.SetValue(control, value, null);
        }

        public override void SetVisible(object control, bool value)
        {
            PropertyInfo info = control.GetType().GetProperty("Visible");
            if (info != null && info.CanWrite)
                info.SetValue(control, value, null);
        }

        public override void HookClickEvent(object control, EventHandler clickHandler)
        {
            EventInfo info = control.GetType().GetEvent("Click");
            if (info != null)
                info.AddEventHandler(control, clickHandler);
        }

        public override void UnhookClickEvent(object control, EventHandler clickHandler)
        {
            EventInfo info = control.GetType().GetEvent("Click");
            if (info != null)
                info.RemoveEventHandler(control, clickHandler);
        }
    }
}
