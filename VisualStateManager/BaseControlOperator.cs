﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualStateManager
{
    public abstract class BaseControlOperator
    {
        protected void SafePerform<T>(object control, Action<T> action)
            where T : class
        {
            var temp = control as T;
            if (temp == null)
                throw new ArgumentException("control");

            action(temp);
        }

        public abstract Type AcceptedType
        {
            get;
        }

        public abstract void SetEnabled(object control, bool value);

        public abstract void SetChecked(object control, bool value);

        public abstract void SetVisible(object control, bool value);

        public abstract void HookClickEvent(object control, EventHandler clickHandler);

        public abstract void UnhookClickEvent(object control, EventHandler clickHandler);
    }
}
