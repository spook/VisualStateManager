﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualStateManager
{
    public delegate void ActionExecutedHandler();

    public class Action : BaseVisualStateManager
    {
        // Private methods -----------------------------------------------------

        private void HandleControlClicked(object sender, EventArgs e)
        {
            Execute();
        }

        // Protected methods ---------------------------------------------------

        protected override void AddingControl(object control)
        {
            base.AddingControl(control);

            ControlOperatorManager.HookClickEvent(control, HandleControlClicked);
        }

        protected void OnActionExecuted()
        {
            if (ActionExecuted != null)
                ActionExecuted();
        }

        protected override void RemovingControl(object control)
        {
            base.RemovingControl(control);

            ControlOperatorManager.UnhookClickEvent(control, HandleControlClicked);
        }

        // Public methods ------------------------------------------------------

        public Action(params object[] controls)
            : base(controls)
        {

        }

        public Action(ActionExecutedHandler newActionExecuted,
            params object[] controls)
            : base(controls)
        {
            ActionExecuted += newActionExecuted;
        }

        public Action(ActionExecutedHandler newActionExecuted,
            BaseCondition enabledCondition,
            params object[] controls)
            : base(enabledCondition, controls)
        {
            ActionExecuted += newActionExecuted;
        }

        public Action(ActionExecutedHandler newActionExecuted,
            BaseCondition enabledCondition,
            BaseCondition checkedCondition,
            params object[] controls)
            : base(enabledCondition, checkedCondition, controls)
        {
            ActionExecuted += newActionExecuted;
        }

        public Action(ActionExecutedHandler newActionExecuted,
            BaseCondition enabledCondition,
            BaseCondition checkedCondition,
            BaseCondition visibleCondition,
            params object[] controls)
            : base(enabledCondition, checkedCondition, visibleCondition, controls)
        {
            ActionExecuted += newActionExecuted;
        }

        public void Execute()
        {
            OnActionExecuted();
        }

        // Public properties ---------------------------------------------------

        public bool CanExecute
        {
            get
            {
                if (enabledCondition != null)
                    return enabledCondition.GetValue();
                else
                    return true;
            }
        }

        public event ActionExecutedHandler ActionExecuted;
    }
}
