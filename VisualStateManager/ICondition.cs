﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualStateManager
{
    public delegate void ValueChangedHandler(object sender, ValueChangedEventArgs e);

    public interface ICondition
    {
        bool Value
        {
            get;
        }

        event ValueChangedHandler ValueChanged;
    }
}
