﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualStateManager
{
    public class ControlOperator : BaseControlOperator
    {
        public override Type AcceptedType
        {
            get
            {
                return typeof(Control);
            }
        }

        public override void SetEnabled(object control, bool value)
        {
            SafePerform<Control>(control, p => p.Enabled = value);
        }

        public override void SetChecked(object control, bool value)
        {
            // Generic Control does not support checked
        }

        public override void SetVisible(object control, bool value)
        {
            SafePerform<Control>(control, p => p.Visible = value);
        }

        public override void HookClickEvent(object control, EventHandler clickHandler)
        {
            SafePerform<Control>(control, p => p.Click += clickHandler);
        }

        public override void UnhookClickEvent(object control, EventHandler clickHandler)
        {
            SafePerform<Control>(control, p => p.Click -= clickHandler);
        }
    }
}
