﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisualStateManager
{
    public class SwitchControlSet<T>
        where T : struct
    {
        public SwitchControlSet(T value, params object[] controls)
        {
            Value = value;
            Controls = controls;
        }

        public T Value
        {
            get;
            private set;
        }

        public object[] Controls
        {
            get;
            private set;
        }
    }

    public class VisualStateSwitchController<T>
        where T : struct
    {
        private readonly Dictionary<T, VisualStateController> controllers;

        public VisualStateSwitchController(params SwitchControlSet<T>[] switchControls)
            : this(null, null, null, switchControls)
        {

        }

        public VisualStateSwitchController(SwitchCondition<T> enabledCondition,
            params SwitchControlSet<T>[] switchControls)
            : this(enabledCondition, null, null, switchControls)
        {

        }

        public VisualStateSwitchController(SwitchCondition<T> enabledCondition,
            SwitchCondition<T> checkedCondition,
            params SwitchControlSet<T>[] switchControls)
            : this(enabledCondition, checkedCondition, null, switchControls)
        {

        }

        public VisualStateSwitchController(SwitchCondition<T> enabledCondition,
            SwitchCondition<T> checkedCondition,
            SwitchCondition<T> visibleCondition,
            params SwitchControlSet<T>[] switchControls)
        {
            controllers = new Dictionary<T, VisualStateController>();

            if (switchControls
                .Select(sc => sc.Value)
                .Distinct()
                .Count() != switchControls.Length)
                throw new ArgumentException("Values must be unique!");

            foreach (var switchControl in switchControls)
            {
                var controller = new VisualStateController(
                    (enabledCondition != null && enabledCondition.Values.Contains(switchControl.Value)) ? enabledCondition[switchControl.Value] : null,
                    (checkedCondition != null && checkedCondition.Values.Contains(switchControl.Value)) ? checkedCondition[switchControl.Value] : null,
                    (visibleCondition != null && visibleCondition.Values.Contains(switchControl.Value)) ? visibleCondition[switchControl.Value] : null,
                    switchControl.Controls);

                controllers.Add(switchControl.Value, controller);
            }
        }
    }
}
